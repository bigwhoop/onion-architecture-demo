<?php

declare(strict_types=1);

namespace domain\OrderManagement;

interface UserRepo
{
    public function find(int $userId): User;
}
