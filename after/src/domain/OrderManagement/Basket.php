<?php

declare(strict_types=1);

namespace domain\OrderManagement;

final class Basket
{
    private $id = 0;
    private $user;

    public function __construct(int $basketId, User $user)
    {
        $this->id = $basketId;
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function changeUser(User $user)
    {
        if (!$user->isVerified()) {
            throw new \LogicException();
        }

        if ($this->user->belongsToSameMerchantAs($user)) {
            throw new \LogicException();
        }

        if ($this->user->isSame($user)) {
            throw new \LogicException();
        }

        $this->user = $user;
    }
}
