<?php

declare(strict_types=1);

namespace domain\OrderManagement;

interface BasketRepo
{
    public function find(int $basketId): Basket;
    public function save(Basket $basket);
}
