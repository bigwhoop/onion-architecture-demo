<?php

declare(strict_types=1);

namespace domain\OrderManagement;

final class User
{
    private $id = 0;
    private $merchantId = 0;

    public function __construct(int $userId, int $merchantId)
    {
        $this->id = $userId;
        $this->merchantId = $merchantId;
    }

    public function belongsToSameMerchantAs(User $user): bool
    {
        return $this->merchantId === $user->merchantId;
    }

    public function isSame(User $user): bool
    {
        return $this->id === $user->id;
    }

    public function isVerified(): bool
    {
        return true;
    }
}
