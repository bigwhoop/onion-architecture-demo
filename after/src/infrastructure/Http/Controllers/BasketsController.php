<?php

declare(strict_types=1);

namespace infrastructure\Http\Controllers;

use application\BasketService;
use Application\Commands\ChangeBasketUserCommand;

/**
 * @property \Request httpRequest
 * @property \CommandBus commandBus
 * @property BasketService $basketService
 */
final class BasketsController
{
    public function changeUserAction()
    {
        // cqrs
        $command = new ChangeBasketUserCommand(
            $this->httpRequest->getQuery('basketId'),
            $this->httpRequest->getQuery('userId')
        );

        $this->commandBus->handle($command);

        // application services
        $basket = $this->basketService->changeBasketUser(
            $this->httpRequest->getQuery('basketId'),
            $this->httpRequest->getQuery('userId')
        );
    }
}
