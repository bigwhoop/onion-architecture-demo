<?php

declare(strict_types=1);

namespace application\Commands;

use Domain\OrderManagement\BasketRepo;
use Domain\OrderManagement\UserRepo;

final class ChangeBasketUserCommandHandler
{
    private $basketRepo;
    private $userRepo;

    public function __construct(BasketRepo $basketRepo, UserRepo $userRepo)
    {
        $this->basketRepo = $basketRepo;
        $this->userRepo = $userRepo;
    }

    public function handle(ChangeBasketUserCommand $command)
    {
        $basket = $this->basketRepo->find($command->getBasketId());
        $user = $this->userRepo->find($command->getUserId());

        $basket->changeUser($user);

        $this->basketRepo->save($basket);
    }
}
