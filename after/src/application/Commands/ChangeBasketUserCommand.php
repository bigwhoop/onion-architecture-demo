<?php

declare(strict_types=1);

namespace application\Commands;

final class ChangeBasketUserCommand
{
    private $basketId = 0;

    public function __construct(int $basketId, int $userId)
    {
        $this->basketId = $basketId;
        $this->userId = $userId;
    }

    public function getBasketId(): int
    {
        return $this->basketId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
