<?php

declare(strict_types=1);

namespace application;

use domain\OrderManagement\Basket;
use Domain\OrderManagement\BasketRepo;
use Domain\OrderManagement\UserRepo;

final class BasketService
{
    private $basketRepo;
    private $userRepo;

    public function __construct(BasketRepo $basketRepo, UserRepo $userRepo)
    {
        $this->basketRepo = $basketRepo;
        $this->userRepo = $userRepo;
    }

    public function changeBasketUser(int $basketId, int $userId): Basket
    {
        $basket = $this->basketRepo->find($basketId);
        $user = $this->userRepo->find($userId);

        $basket->changeUser($user);

        $this->basketRepo->save($basket);

        return $basket;
    }
}
