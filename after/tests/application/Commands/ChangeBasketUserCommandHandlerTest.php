<?php

declare(strict_types=1);

namespace TestsOrderManagement;

use Application\Commands\ChangeBasketUserCommand;
use Application\Commands\ChangeBasketUserCommandHandler;
use Domain\OrderManagement\Basket;
use Domain\OrderManagement\BasketRepo;
use Domain\OrderManagement\User;
use Domain\OrderManagement\UserRepo;

final class ChangeBasketUserCommandHandlerTest extends \TestCase
{
    /** @var ChangeBasketUserCommandHandler */
    private $handler;

    public function setUp()
    {
        $this->handler = new ChangeBasketUserCommandHandler(new StaticBasketRepo(), new StaticUserRepo());
    }

    /** @test */
    public function i_can_change_user_that_belongs_to_same_merchant()
    {
        $this->handler->handle(new ChangeBasketUserCommand(100, 201));
    }

    /**
     * @test
     * @expectedException \LogicException
     */
    public function i_can_not_change_user_that_belongs_to_a_different_merchant()
    {
        $this->handler->handle(new ChangeBasketUserCommand(100, 202));
    }
}

// ---

final class StaticBasketRepo implements BasketRepo
{
    private $basketsById = [];

    public function __construct()
    {
        $this->basketsById = [
            100 => new Basket(100, new User(200, 300)),
        ];
    }

    public function find(int $basketId): Basket
    {
        if (!array_key_exists($basketId, $this->basketsById)) {
            throw new \NoRecordFoundException();
        }

        return $this->basketsById[$basketId];
    }

    public function save(Basket $basket)
    {
        $this->basketsById[$basket->getId()] = $basket;
    }
}

final class StaticUserRepo implements UserRepo
{
    private $usersById = [];

    public function __construct()
    {
        $this->usersById = [
            200 => new User(200, 300),
            201 => new User(201, 300),
            202 => new User(202, 301),
        ];
    }

    public function find(int $userId): User
    {
        if (!array_key_exists($userId, $this->usersById)) {
            throw new \NoRecordFoundException();
        }

        return $this->usersById[$userId];
    }
}
