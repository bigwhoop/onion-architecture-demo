<?php

declare(strict_types=1);

namespace tests\domain\OrderManagement;

use Domain\OrderManagement\Basket;
use Domain\OrderManagement\User;

final class BasketTest extends \TestCase
{
    /** @test */
    public function i_can_change_to_user_that_belongs_to_same_merchant()
    {
        $basket = new Basket(100, new User(200, 300));
        $basket->changeUser(new User(201, 300));
    }

    /**
     * @test
     * @expectedException \LogicException
     */
    public function i_can_not_change_to_user_that_belongs_to_a_different_merchant()
    {
        $basket = new Basket(100, new User(200, 300));
        $basket->changeUser(new User(201, 301));
    }

    /**
     * @test
     * @expectedException \LogicException
     */
    public function i_can_not_change_to_same_user()
    {
        $basket = new Basket(100, new User(200, 300));
        $basket->changeUser(new User(200, 300));
    }
}
