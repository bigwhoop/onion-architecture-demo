# After

- We can now test the domain in isolation.
- We can trigger commands from all kinds of transports and places.
    - We can even go async and make the command handler send the command to a queue.
    - I used a command bus. But we could also write application services
- Our application (our use cases, user interactions) are no longer bound to implementation
  details like DBs or network protocols.