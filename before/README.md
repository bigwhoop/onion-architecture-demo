# Before

- How to add support for other transports like CLI or a queue?
- How to test it?
- What to test? What is really important here?
    - Is it the HTTP/controller stuff?
    - Is it the persistence?
    - It's the adherence to the (business) rules.
        - You MUST change the user to a user of the same merchants
        - You MUST NOT change the user to the same user
        - You MUST change to a verified user only.
    - They are mix in with applocation code.