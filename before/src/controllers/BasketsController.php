<?php

declare(strict_types=1);

namespace controllers;

/**
 * @method \Request getHttpRequest()
 * @method \EntityLoader getEntityLoader()
 */
final class BasketsController
{
    public function changeUserAction()
    {
        $request = $this->getHttpRequest();

        $basket = $this->getEntityLoader()->getBasketForModification($request->getQuery('basketId'));
        $user = $this->getEntityLoader()->getUserForModification($request->getQuery('basketId'));

        if ($user->mandatorId !== $this->getEntityLoader()->getCurrentUser()->mandatorId) {
            throw new \Trekksoft_Controller_Exception_BadRequest('...');
        }

        $basket->user = $user;
        $basket->save();
    }
}
