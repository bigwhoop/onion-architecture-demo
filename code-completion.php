<?php declare(strict_types=1);

class Trekksoft_Controller_Exception_BadRequest extends \Exception
{}

interface EntityLoader
{
    public function getBasketForModification(int $basketId): \Models\Basket;
    public function getUserForModification(int $userId): \Models\User;
    public function getCurrentUser(): \Models\User;
}

interface Request
{
    public function getQuery(string $key);
}

interface CommandBus
{
    public function handle($command);
}

final class NoRecordFoundException extends \RuntimeException
{}

class TestCase
{}